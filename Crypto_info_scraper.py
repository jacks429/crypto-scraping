import re
import json
import random
import requests
import datetime
import os.path
from timeloop import Timeloop
from datetime import timedelta
from bs4 import BeautifulSoup
from lxml import html
import logging

LOG_FILENAME = f"{os.path.split(os.getcwd())[0]}\etc\listings.out"
logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG)

tl = Timeloop()
headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'}
req_session = requests.Session()

listings = open(os.path.join('database', "exchange_listings.json"), 'r+')


@tl.job(interval=timedelta(seconds=30))
def grab_kucoin_listing():
    try:
        feed = req_session.get("https://www.kucoin.com/rss/news?lang=en")
        soup = BeautifulSoup(feed.content, features='xml')
        listings_j = json.load(listings)
        kucoin_list = listings_j["exchanges"]["kucoin"]

        for item in soup.findAll("item"):
            title = item.find("title").text
            discription = item.find("description").text
            link = item.find("link").text
            day = str(item.find("pubDate").text)
            coin = re.search(r'\(.*?\)', str(title))
            if coin:

                coin_s = str(coin).strip("()")
                if "listed" in str(title).lower() and not any(
                        d["coin"] == coin_s for d in kucoin_list) and int(day.split(" ")[1]) == int(datetime.date.today().day):
                    kucoin_list.append(
                        {"coin": str(coin), "title": title, "discription": discription, "link": link, "date": day,
                         "status": "not"})
                    listings.seek(0)
                    listings.truncate()
                    json.dump(listings_j, listings, indent=2)
    except Exception as e:
        logging.exception(str(e))


@tl.job(interval=timedelta(minutes=random.randint(5, 15)))
def coinmarketcap_listing():
    try:
        old = open(os.path.join("database/old_coins.txt"), "r")
        req = req_session.get("https://coinmarketcap.com/new/")
        tree = html.fromstring(req.content)
        coin = tree.xpath("(//p[contains(@font-weight,'semibold')])/text()")
        coin_short_hand = tree.xpath("(//p[contains(@class,'coin-item-symbol')])/text()")
        time_ = tree.xpath(
            "//body[contains(@class,'DAY')]/div[@id='__next']/div[contains(@class,'bywovg-1 sXmSU')]/"
            "div[contains(@class,'main-content')]/div[contains(@class,'sc-57oli2-0 comDeo cmc-body-wra"
            "pper')]/div[contains(@class,'grid')]/div[contains(@class,'h7vnx2-1 bFzXgL')]/table[contain"
            "s(@class,'')]/tbody/tr/td[10]/text()")

        with open(os.path.join("database", "coinmarketcap.txt"), "w+") as f:
            for coin in list(zip(coin, coin_short_hand, time_)):
                if "minutes" in coin[2] and coin[1] not in old.read():
                    f.write(f"{coin[0]}:{coin[1]}\n")
            f.close()
    except Exception as e:
        logging.exception(str(e))


def bankcex():
    req = req_session.get("https://info.bankcex.com/project-list.html")
    tree = html.fromstring(req.content)
    coin = tree.xpath(
        "//body[@class='ng-scope']/div[@class='ng-scope']/div[@class='wrapper ng-scope']/div[@class='l"
        "abs project-list-page project-list-page-new']/div[@class='container']/div[@class='row']/div[@"
        "class='col-md-12']/md-content[@class='md-content-new _md']/md-tabs[@class='ng-isolate-scope md"
        "-dynamic-height']/md-tabs-content-wrapper[@class='_md']/md-tab-content[@id='tab-content-0']/div"
        "[@class='ng-scope ng-isolate-scope']/md-content[@class='ng-scope _md']/div[@class='tab-pane tabl"
        "e-responsive']/div/div[1]/text()")


tl.start(block=True)
